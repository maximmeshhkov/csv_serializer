﻿using System;

namespace csv_serializer.DataClasses
{
    public class Person
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string HairStyle { get; set; }
    }
}
