﻿using System;

namespace csv_serializer.DataClasses
{
    public class CustomAccount : IAccount
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime? BirthDate { get; set; }
    }

    public interface IAccount
    {
        string FirstName { get; set; }
        string LastName { get; set; } 
        DateTime? BirthDate { get; set; }
    }
}
