﻿using System.Collections.Generic;

namespace csv_serializer.Sorter
{
    public interface ISorter<TClass> where TClass : class
    {
        IEnumerable<TClass> SortByName(IEnumerable<TClass> notSortedItems);
    }
}
