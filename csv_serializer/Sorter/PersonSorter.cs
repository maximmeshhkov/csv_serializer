﻿using System.Collections.Generic;
using System.Linq;
using csv_serializer.DataClasses;

namespace csv_serializer.Sorter
{
    //TODO: this is a mistake to do such things, because it's better to move to the class HelperExtension (09/11/2020 Maxim Meshkov)
    public class PersonSorter : ISorter<Person>
    {
        //TODO: ove to Helper class and make thus part fluent! => this IEnumerable<Person> notSortedItems
        public IEnumerable<Person> SortByName(IEnumerable<Person> notSortedItems)
        {
            return notSortedItems.OrderByDescending(x => x.Name);
        }
    }
}
