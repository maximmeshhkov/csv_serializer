﻿using System.Collections.Generic;
using System.IO;

namespace csv_serializer.Serializer
{
    public interface ISerializer<TClass> where TClass : class 
    {
        void Serialize(string path = null, params TClass[] data);

        IEnumerable<TClass> DeSerialize(string data);

        IEnumerable<TClass> DeSerialize(StreamReader reader);
    }
}
