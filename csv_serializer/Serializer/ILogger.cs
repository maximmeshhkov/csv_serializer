﻿using System;
using System.Collections.Generic;

namespace csv_serializer.Serializer
{
    public interface ILogger
    {
        public Lazy<IList<string>> Errors { get; }

        public void LogError(string ex);
        
        //Clean was on users hand, but in current "on dollar" implementation its done after each and every operation
        public void Clean();
    }
}
