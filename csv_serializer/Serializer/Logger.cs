﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace csv_serializer.Serializer
{
    public class Logger : ILogger
    {
        public Lazy<IList<string>> Errors { get; } = new Lazy<IList<string>>(() => new List<string>());

        public void LogError(string ex)
        {
            Errors.Value.Add(ex ?? "Exception doesn't have message inside.");
        }

        public void Clean()
        {
            if (Errors.Value.Any())
                Errors.Value.Clear();
        }
    }
}
