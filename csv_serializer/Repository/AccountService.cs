﻿using System;
using System.Collections.Generic;
using csv_serializer.DataClasses;
using csv_serializer.Serializer;

namespace csv_serializer.Repository
{
    public class AccountService : IAccountService
    {
        private readonly IRepository<CustomAccount> _repository;
        public CustomAccount LatestAddedAccount { get; private set; }

        public AccountService(IRepository<CustomAccount> repository)
        {
            _repository = repository ?? new Repository<CustomAccount>(new Logger(), null);
        }

        public void AddAccount(CustomAccount account)
        {
            if (!ValidateAccount(account)) return;
            _repository.Add(account);
            LatestAddedAccount = account;
        }

        public CustomAccount GetAccount(Func<CustomAccount, bool> predicate)
        {
            _repository.Logger.Clean();
            return _repository.GetOne(predicate);
        }

        //its fine if we want validate account without attempt to add it
        public bool ValidateAccount(CustomAccount account)
        {
            _repository.Logger.Clean();
           
            if (account?.BirthDate == null)
            {
                const string ex = "Account or its BirthDate cannot be null.";
                _repository.Logger.LogError(ex);
                throw new Exception(ex);
            }

            if (DateTime.Now.Year - account.BirthDate.Value.Year < 18)
            {
                const string ex = "User must be more then 18 years old.";
                _repository.Logger.LogError(ex);
                throw new Exception(ex);
            }

            return true;
        }

        public IList<string> GetErrors()
        {
            return _repository.Logger.Errors.Value;
        }
    }
}
